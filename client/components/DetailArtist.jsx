// DetailArtist component - represents details of an artist  
DetailArtist = React.createClass({  

// This mixin makes the getMeteorData method work
  mixins: [ReactMeteorData],

  // Loads items from collection and puts them on this.data
  getMeteorData() {
    return {
      artist : Artistes.findOne({_id:this.props.id}),//artist sera accessible dans le render
      currentUser: Meteor.user()
    }
  },

  render() {
    return (
      <div className='detail-artist'>
        <h1> Detail artist </h1>
         <p><b>_id:</b> {this.data.artist._id}</p>
         <p><b>Name:</b> {this.data.artist.name}</p>
         <p><b>Style</b> de music:{this.data.artist.musicstyle}</p>
         <p><b>Vote:</b> {this.data.artist.vote}</p>
      </div>
    )
  }
});
