

if (Meteor.isClient) {

  Accounts.ui.config({
    passwordSignupFields: "USERNAME_ONLY"
  });

  Meteor.subscribe("artistes");
  Meteor.subscribe("musicstyles");

 // This code is executed on the client only
 // Meteor.startup(function () {
 //   // Use Meteor.startup to render the component after the page is ready
 //   ReactDOM.render(<App />, document.getElementById("render-target"));
 // });
}


if (Meteor.isServer) {

  Meteor.startup(function () {//au lancement de meteor (commande meteor)
  	
  	console.log('Hello server side!');
	
	//Fixtures
	if (Artistes.find().count() === 0) {
	  console.log('count artists === 0');	
	  Artistes.insert({ name: 'Damien Saez', musicstyle:'Chanson francaise' ,vote: 2 });
	  Artistes.insert({ name: 'Francis Lalanne', musicstyle:'Chanson francaise' ,vote: 5 });
	  Artistes.insert({ name: 'Jimi Hendrix', musicstyle:'Rock' ,vote: 3 });
	}

	if (MusicStyles.find().count() === 0) {
	  console.log('count music style === 0');	
	  MusicStyles.insert({ musicstyle: 'Rock' });
	  MusicStyles.insert({ musicstyle: 'Pop' });
	  MusicStyles.insert({ musicstyle: 'Classic' });
	  MusicStyles.insert({ musicstyle: 'Chanson francaise' });
	}

  });

  Meteor.publish("artistes", function () {
    return Artistes.find();
  });
  Meteor.publish("musicstyles", function () {
    return MusicStyles.find();
  });

}