
// LandingPage component 

Home = React.createClass({  

// This mixin makes the getMeteorData method work
  mixins: [ReactMeteorData],

  // Loads items from collection and puts them on this.data
  getMeteorData() {
    var query = this.state.value === 'All' ? {} : { musicstyle:this.state.value, vote:{$gt:1}};

    return {
      artistes: Artistes.find(query , {sort: {vote: -1}}).fetch(),
      artistCount: Artistes.find(query).count(),
      topArtist: Artistes.find(query, {sort: {vote: -1}, limit: 1}).fetch(),
      musicstyles: MusicStyles.find({}).fetch(),
      musicstylesCount: MusicStyles.find({}).count(),
      currentUser: Meteor.user()
    }
  },

  renderArtists() {
    return this.data.artistes.map((artist) => {
      return <Artist key={artist._id} artist={artist} />;
    });
  },

  renderMusicStylesFiltre() {
    return this.data.musicstyles.map((musicstyle) => {
      return <MusicStyle key={musicstyle._id} musicstyle={musicstyle} />;
    });
  },

  //Menu deroulant style de music
  getInitialState() {
    return {
      value: 'All'
    }
  },

  onChangeFiltre(event){
    //console.log(event.target.value);
    this.setState({value: event.target.value});
  }, 

  //Ajout d'un nouvel artiste
  handleSubmit(event) {
    event.preventDefault();

    // Find the text field via the React ref
    var name = ReactDOM.findDOMNode(this.refs.artistName).value.trim();

    Meteor.call("addArtist", name);//voir dans Meteor.methods({..

    // Clear form
    ReactDOM.findDOMNode(this.refs.textInput).value = "";
  },
  //pour le style dans les balises on utilise className au lieu de class




  render() {
    let nbrArtistTxt =  'Nombre d\'artistes';
    //console.log('this.data.topArtist.name:',this.data.topArtist);
    let topArtist = typeof this.data.topArtist[0] != 'undefined' ? this.data.topArtist[0] : '???' ;
    //console.log('render');
    return (

      <div className="body">
        <h1>Welcome to the Wall!</h1>

          { this.data.currentUser ?
            <form className="new-artist" onSubmit={this.handleSubmit} >
              <h2 className='inline'>Ajouter un artiste </h2>
              <input
                type="text"
                ref="artistName"
                placeholder="Artist name" />
                
            </form> : ''
          } 

          <h2> {nbrArtistTxt}:  
            <span className='blue'>{this.data.artistCount}</span>
          </h2>

          <h2 >Le meilleur est : 
            <span id='top-artist' className='blue'>{topArtist.name}</span>
          </h2>

          <h2 >Filtre par style de music: 
            <select id="musicstylesFiltre" onChange={this.onChangeFiltre} value={this.state.value}>
              <option value='All'>All</option>
              {this.renderMusicStylesFiltre()}
            </select>
          </h2>

          {this.renderArtists()}    

 		  </div>
    )
  }
});

Test = React.createClass({  
  render() {
    return (
      <div className="footer">
        <h1> test !</h1>
      </div>
    )
  }
});
