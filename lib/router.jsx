
FlowRouter.route('/', {  
	name:'home',	
	action() {
		ReactLayout.render(MainLayout, { content: <Home /> });
	}
});


FlowRouter.route('/test', {  
  name:'test',	
  action() {
    ReactLayout.render(MainLayout, { content: <Test /> });
  }
});


FlowRouter.route('/detailArtist/:id', {  
  name:'detailArtist',
  action(params, queryParams) {
  	console.log("Yeah! We are on the post:", params.id);
    //ReactLayout.render(MainLayout, { content: <DetailArtist /> });
    ReactLayout.render(MainLayout, { content: <DetailArtist {...params} /> });
  }
});


