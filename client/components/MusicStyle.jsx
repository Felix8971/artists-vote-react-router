// MusicStyle component - represents a single music style 
MusicStyle = React.createClass({
  propTypes: {
    // This component gets the musicStyle to display through a React prop.
    // We can use propTypes to indicate it is required
    //artist: React.PropTypes.object.isRequired
    musicstyle: React.PropTypes.object.isRequired

  },

  //pour le style dans les balises on utilise className au lieu de class
  //pour utiliser les attributs props dans les attribus de balise la syntaxe react est assez particuliere 

  render() {
    return (
    <option value={this.props.musicstyle.musicstyle}>{this.props.musicstyle.musicstyle}</option>
    );
  }

});

