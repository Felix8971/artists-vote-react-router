Meteor.methods({

	voter(artistId, inc) {
		if (! Meteor.userId()) {
			throw new Meteor.Error("not-authorized");
		}
	  	var curVal = Artistes.findOne({_id:artistId},{vote:1}).vote;
	  	console.log("val:"+curVal);
	  	if (  inc > 0 || ( inc < 0 && curVal > 0) ){
	    	Artistes.update(artistId, {
	      	$set: {vote: curVal + inc }
	    	});
		}
	},


	addArtist(name) {
		// Make sure the user is logged in before inserting an artist 
		if (! Meteor.userId()) {
		  throw new Meteor.Error("not-authorized");
		}
		Artistes.insert({
		  name: name,
		  vote:0,
		  createdAt: new Date(),
		  owner: Meteor.userId(),
		  username: Meteor.user().username
		});
	},


    removeArtist(artistId){
    	if (! Meteor.userId()) {
		  throw new Meteor.Error("not-authorized");
		}
    	Artistes.remove(artistId);
    },

  // setChecked(taskId, setChecked) {
  //   Tasks.update(taskId, { $set: { checked: setChecked} });
  // }

});

