// Artist component - represents a single artist 
Artist = React.createClass({
  propTypes: {
    // This component gets the artist to display through a React prop.
    // We can use propTypes to indicate it is required

    artist: React.PropTypes.object.isRequired
  },


  voteForThisArtist() {
    if ( Meteor.user() ){
      Meteor.call("voter",this.props.artist._id, 1);
    }else{
      alert('Vous devez etre connecté pour voter !');
    } 
  },

  unVoteForThisArtist() {
    if ( Meteor.user() ){
      Meteor.call("voter",this.props.artist._id, -1);
    }else{
      alert('Vous devez etre connecté pour voter !');
    }      
  },

   //pour le style dans les balises on utilise className au lieu de class
   //pour utiliser les attributs props dans les attribus de balise la syntaxe react est assez particuliere 

  render() {

    return (
      <div className="container">  

           { /*this.props.artist.musicstyle === 'Chanson francaise'*/ true  ? 
            <div className='artist' >
              <div className='name'>
                {this.props.artist.name}: <span className='vote'>{this.props.artist.vote}</span>
              </div>
              
              <button className='plus' id={'plus-button-' + this.props.artist.name}  onClick={this.voteForThisArtist}>+</button>
              <button className='moins' id={'moins-button-' + this.props.artist.name}  onClick={this.unVoteForThisArtist}>-</button>
            
              <a href={'./detailArtist/' + this.props.artist._id } >
                <img src={'./img/' + this.props.artist.name + '.jpg'} className='photo'  />
              </a>
              <hr/>
            </div>
          
            : '' } 

      </div>
    );
  }

});

